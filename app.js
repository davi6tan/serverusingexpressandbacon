var Bacon = require("baconjs").Bacon;
var express = require("express");
var app = express();
var fs = require("fs");
var _ = require('lodash')

app.use(express.static(__dirname + "/public"));
//app.use(express.logger('dev'));
//app.listen(8080);
app.listen(process.env.PORT || 8080);
console.log("Listening to ",8080)

//// initialize /////
function route_eventstream(path) {
  	var bus = new Bacon.Bus();

  	app.get(path, function(req, res) {
    	bus.push({
      		req: req,
      		res: res
    	});
  	});

  	return bus;
}
var root_stream = route_eventstream("/");
root_stream.onValue(function(event){
	event.res.sendFile(__dirname + "/public/html/index.html");
})


var rawdata = Bacon.fromNodeCallback(fs.readFile, "db.json", "utf8").map(event => JSON.parse(event)).toProperty();

function getData(param,paramId){

    var list_stream = route_eventstream("/"+ param);

    function findMatchingParamId(list, targetId) {return _.filter(list, token => token["id"] == targetId)}

    var list_data_stream = Bacon.combineAsArray([rawdata, list_stream])
        .map(event => paramId ? findMatchingParamId(event[0][param], event[1].req.query[paramId]) : event[0])
.toEventStream();

    Bacon.zipAsArray(list_stream, list_data_stream).onValues((event1, event2) => event1.res.send(event2))
}

//http://localhost:8080/all
getData("all","") // get all
//http://localhost:8080/dishes?id=1
getData("dishes","id")
//http://localhost:8080/leadership?id=1
getData("leadership","id")
getData("promotions","id")
getData("feedback","id")